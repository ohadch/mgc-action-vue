import instance from "../instance"

export default async function getActionConfiguration(__id) {
    let {
        data: action
    } = await instance.get("/action/configuration", {
        params: {
            __id
        }
    });
    return action;
}