import instance from '../instance';

export default async function fetchAvailablePilots(actionId, available=true) {
    let {data: pilots} = await instance.get('/available', { params: { actionId, fkName: 'dragPilot', available } });
    return pilots;
}