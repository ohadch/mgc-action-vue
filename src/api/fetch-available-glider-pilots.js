import instance from '../instance';

export default async function fetchAvailableGliderPilots(actionId, available=true) {
    let {data: pilots} = await instance.get('/available', { params: { actionId, fkName: 'pilot1', available } });
    return pilots;
}