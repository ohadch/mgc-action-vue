import instance from '../instance';

export default async function fetchAvailablePilots(actionId, available=true) {
    let {data: airplanes} = await instance.get('/available', { params: { actionId, fkName: 'dragPlane', available } });
    return airplanes;
}