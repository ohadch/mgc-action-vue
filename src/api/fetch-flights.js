import instance from '../instance';

export default async function fetchFlights(actionId) {
    let {data: flights} = await instance.get('/flight', { params: { actionId } });
    for (let flight of flights) {
        if (flight.__pilot1.firstname) {
            flight.__pilot1.__str = flight.__pilot1.firstname + ' ' + flight.__pilot1.lastname
        }
        if (flight.__pilot2.firstname) {
            flight.__pilot2.__str = flight.__pilot2.firstname + ' ' + flight.__pilot2.lastname
        }
        if (flight.__dragpilot.firstname) {
            flight.__dragpilot.__str = flight.__dragpilot.firstname + ' ' + flight.__dragpilot.lastname
        }
    }
    return flights;
}