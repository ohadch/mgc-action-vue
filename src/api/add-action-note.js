import instance from '../instance';

export default async function addActionNote(actionId, content) {
    let {
        data: note
    } = await instance.post('/action/notes', {
        action_id: actionId,
        note: {
            content
        }
    });
    return note;
}