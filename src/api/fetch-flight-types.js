import instance from '../instance';

export default async function fetchFlightTypes() {
    let {data: flightTypes} = await instance.get('/model/data', { params: { modelName: 'flighttype' } });
    return flightTypes;
}