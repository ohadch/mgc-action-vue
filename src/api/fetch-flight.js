import instance from "../instance";

export default async function fetchFlight(flightId) {
    let {
        data: flights
    } = await instance.get('/flight/' + flightId);
    return flights;
}