import instance from '../instance';

export default async function fetchAvailablePilots(actionId, available=true) {
    let {data: gliders} = await instance.get('/available', { params: { actionId, fkName: 'glider', available } });
    return gliders;
}