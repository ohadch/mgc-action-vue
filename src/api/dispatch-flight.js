import instance from '../instance';

export default async function dispatchFlight({
    pilot1Id,
    pilot2Id,
    dragPilotId,
    gliderId,
    dragPlaneId,
    flightTypeId,
    takeOffTime,
    actionId
}) {
    let {
        data: pilots
    } = await instance.post('/flight', {
        Pilot1NameIDSC: pilot1Id,
        Pilot2NameIDSC: pilot2Id,
        DragPilotIDSC: dragPilotId,
        RadioCallIDSC: gliderId,
        DragPlaneRadioCallIDSC: dragPlaneId,
        FlightTypeIDSC: flightTypeId,
        TakeOffTime: takeOffTime,
        __action_id: actionId,
        __state: "tow"
    });
    return pilots;
}