import instance from "../instance"

export default async function getAction(date) {
    let {data: action} = await instance.get("/action", {params: {dateStr: date.toISOString()}});
    return action;
}