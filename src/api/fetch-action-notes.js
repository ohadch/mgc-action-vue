import instance from '../instance';

export default async function fetchActionNotes(actionId) {
    let {data: notes} = await instance.get('/action/notes', {params: {__id: actionId}});
    return notes;
}