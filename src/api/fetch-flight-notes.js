import instance from '../instance';

export default async function fetchFlightNotes(flightId) {
    let {data: notes} = await instance.get('/flight/notes', {params: {__id: flightId}});
    return notes;
}