// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import Loading from 'vue-loading-overlay';
import VModal from 'vue-js-modal'
import Vuetify from 'vuetify'
import VuetifyConfirm from 'vuetify-confirm'
import {store} from './store'

import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vue-loading-overlay/dist/vue-loading.css';

Vue.use(Vuetify);
Vue.use(VuetifyConfirm);
Vue.use(Loading);
Vue.use(VModal, {dynamic: true});
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
});
